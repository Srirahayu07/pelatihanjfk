@extends('layout.master')
@section('content')
<div class="adminx-main-content">
  <div class="container-fluid">
    <!-- BreadCrumb -->
    <nav aria-label="breadcrumb" role="navigation">
      <ol class="breadcrumb adminx-page-breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item"><a href="/pelatihan">Pelatihan</a></li>
        <li class="breadcrumb-item"><a href="/pelatihan/peserta">Tambah Usulan</a></li>
        <li class="breadcrumb-item active" aria-current="page">Edit</li>
      </ol>
    </nav>

    <div class="pb-3">
      <h1>Edit Usulan</h1>
    </div>
<form action="/pelatihan/peserta/{{$peserta->id}}/update" method="post">
                {{csrf_field()}}
                <div class="form-group">
                  <label class="form-label">NIP</label>
                  <input value="{{$peserta->nip}}" name="nip" class="form-control mb-2 input-credit-card" type="text" >
                </div>
                <div class="form-group">
                  <label class="form-label">Nama</label>
                  <input value="{{$peserta->nama}}" name="nama" class="form-control mb-2 input-credit-card" type="text">
                </div>

                <div class="form-group">
                  <label class="form-label">TMT PNS</label>
                  <input value="{{$peserta->tmt_pns}}" name="tmt_pns" class="form-control input-date mb-2" type="text" >
                </div>
                <div class="form-group">
                  <label class="form-label">SK CPNS</label>
                  <input value="{{$peserta->sk_cpns}}" name="sk_cpns" class="form-control input-date mb-2" type="text" >
                </div>
                <div class="form-group">
                  <label class="form-label">SK PNS</label>
                  <input value="{{$peserta->sk_pns}}" name="sk_pns" class="form-control input-date mb-2" type="text" >
                </div>
                <div class="form-group">
                  <label class="form-label">Pangkat</label>
                  <input value="{{$peserta->pangkat}}" name="pangkat" class="form-control input-date mb-2" type="text" >
                </div>
                <div class="form-group">
                  <label class="form-label">SK KP Terakhir</label>
                  <input value="{{$peserta->sk_kp_terakhir}}" name="sk_kp_terakhir" class="form-control input-date mb-2" type="text">
                </div>
                <div class="form-group">
                  <label class="form-label">Unit Kerja</label>
                  <input value="{{$peserta->unit_kerja}}" name="unit_kerja" class="form-control input-date mb-2" type="text">
                </div>
                <div class="form-group">
                  <label class="form-label">Pendidikan Terakhir</label>
                  <input value="{{$peserta->pendidikan_terakhir}}" name="pendidikan_terakhir" class="form-control input-date mb-2" type="text">
                </div>
                <div class="form-group">
                  <label class="form-label">Ijazah</label>
                  <input value="{{$peserta->ijazah}}" name="ijazah" class="form-control input-date mb-2" type="text">
                </div>
                <div class="form-group row">
                  <label for="jabatan" class="col-sm-2 col-form-label">Jabatan</label>
                  <div class="col-3">
                    <select name="jabatan" class="form-control" id="jabatan">
                      <option value="Semua Jabatan" @if($peserta->jabatan == 'Semua Jabatan') selected @endif>Semua Jabatan</option>
                      <option value="Analis Kepegawaian" @if($peserta->jabatan == 'Analisis Kepegawaian') selected @endif>Analis Kepegawaian</option>
                      <option value="Auditor Kepegawaian" @if($peserta->jabatan == 'Auditor Kepegawaian') selected @endif>Auditor Kepegawaian</option>

                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="jenis_pelatihan" class="col-sm-2 col-form-label">Jenis Pelatihan</label>
                  <div class="col-3">
                    <select name="jenis_pelatihan" class="form-control" id="jenis_pelatihan">
                      <option value="Diklat Fungsional Analis Kepegawaian Keterampilan"  @if($peserta->jenis_pelatihan == 'Diklat Fungsional Analis Kepegawaian Keterampilan') selected @endif>Diklat Fungsional Analis Kepegawaian Keterampilan</option>
                        <option value="Diklat Fungsional Analis Kepegawaian Keahlian"  @if($peserta->jenis_pelatihan == 'Diklat Fungsional Analis Kepegawaian Keahlian') selected @endif>Diklat Fungsional Analis Kepegawaian Keahlian</option>
                          <option value="Diklat Fungsional Auditor Kepegawaian"  @if($peserta->jenis_pelatihan == 'Diklat Fungsional Auditor Kepegawaian') selected @endif>Diklat Fungsional Auditor Kepegawaian</option>
                            <option value="Diklat Fungsional Assesor SDM Aparatur"  @if($peserta->jenis_pelatihan == 'Diklat Fungsional Assesor SDM Aparatur') selected @endif>Diklat Fungsional Assesor SDM Aparatur</option>
                              <option value="Diklat Teknis Kenaikan Pangkat"  @if($peserta->jenis_pelatihan == 'Diklat Teknis Kenaikan Pangkat') selected @endif>Diklat Teknis Kenaikan Pangkat</option>
                                <option value="Diklat Teknis Kompensasi"  @if($peserta->jenis_pelatihan == 'Diklat Teknis Kompensasi') selected @endif>Diklat Teknis Kompensasi</option>
                                  <option value="Diklat Teknis Tata Usaha Kepegawaian"  @if($peserta->jenis_pelatihan == 'Diklat Teknis Tata Usaha Kepegawaian') selected @endif>Diklat Teknis Tata Usaha Kepegawaian</option>
                                    <option value="Diklat Teknis Disiplin PNS"  @if($peserta->jenis_pelatihan == 'Diklat Teknis Disiplin PNS') selected @endif>Diklat Teknis Disiplin PNS</option>
                                      <option value="Diklat Teknis Pemberhentian dan Pensiun PNS"  @if($peserta->jenis_pelatihan == 'Diklat Teknis Pemberhentian dan Pensiun PNS') selected @endif>Diklat Teknis Pemberhentian dan Pensiun PNS</option>
                                        <option value="Diklat Teknis Pengadaan PNS"  @if($peserta->jenis_pelatihan == 'Diklat Teknis Pengadaan PNS') selected @endif>Diklat Teknis Pengadaan PNS</option>
                                          <option value="Diklat Teknis Pengembangan Karier PNS"  @if($peserta->jenis_pelatihan == 'Diklat Teknis Pengembangan Karier PNS') selected @endif>Diklat Teknis Pengembangan Karier PNS</option>
                                            <option value="Diklat Teknis Penyusunan Analisis Jabatan"  @if($peserta->jenis_pelatihan == 'Diklat Teknis Penyusunan Analisis Jabatan') selected @endif>Diklat Teknis Penyusunan Analisis Jabatan</option>
                                              <option value="Diklat Teknis Penyusunan Standarisasi Jabatan PNS"  @if($peserta->jenis_pelatihan == 'Diklat Teknis Penyusunan Standarisasi Jabatan PNS') selected @endif>Diklat Teknis Penyusunan Standarisasi Jabatan PNS</option>
                                                <option value="Diklat Teknis Penilaian dan Penetapan Angka Kredit"  @if($peserta->jenis_pelatihan == 'Diklat Teknis Penilaian dan Penetapan Angka Kredit') selected @endif>Diklat Teknis Penilaian dan Penetapan Angka Kredit</option>
                                                  <option value="Diklat Teknis Penyusunan Rencana Kerja Wasdalpeg"  @if($peserta->jenis_pelatihan == 'Diklat Teknis Penyusunan Rencana Kerja Wasdalpeg') selected @endif>Diklat Teknis Penyusunan Rencana Kerja Wasdalpeg</option>
                                                    <option value="Diklat Teknis Pelaksanaan Wasdalpeg"  @if($peserta->jenis_pelatihan == 'Diklat Teknis Pelaksanaan Wasdalpeg') selected @endif>Diklat Teknis Pelaksanaan Wasdalpeg</option>
                                                      <option value="Diklat Teknis Pelaporan Hasil Wasdalpeg"  @if($peserta->jenis_pelatihan == 'Diklat Teknis Pelaporan Hasil Wasdalpeg') selected @endif>Diklat Teknis Pelaporan Hasil Wasdalpeg</option>
                                                        <option value="Diklat Teknis Pemantauan Tindak Lanjut Hasil Wasdalpeg"  @if($peserta->jenis_pelatihan == 'Diklat Teknis Pemantauan Tindak Lanjut Hasil Wasdalpeg') selected @endif>Diklat Teknis Pemantauan Tindak Lanjut Hasil Wasdalpeg</option>
                                                          <option value="Diklat Teknis Evaluasi Bidang Wasdalpeg"  @if($peserta->jenis_pelatihan == 'Diklat Teknis Evaluasi Bidang Wasdalpeg') selected @endif>Diklat Teknis Evaluasi Bidang Wasdalpeg</option>
                                                            <option value="Diklat Teknis Pembuatan Karya Tulis/Karya Ilmiah Bidang Wasdalpeg"  @if($peserta->jenis_pelatihan == 'Diklat Teknis Pembuatan Karya Tulis/Karya Ilmiah Bidang Wasdalpeg') selected @endif>Diklat Teknis Pembuatan Karya Tulis/Karya Ilmiah Bidang Wasdalpeg</option>
                                                              <option value="Diklat Penjenjangan Tingkat I"  @if($peserta->jenis_pelatihan == 'Diklat Penjenjangan Tingkat I') selected @endif>Diklat Penjenjangan Tingkat I</option>
                                                                <option value="Diklat Penjenjangan Tingkat II"  @if($peserta->jenis_pelatihan == 'Diklat Penjenjangan Tingkat II') selected @endif>Diklat Penjenjangan Tingkat II</option>
                                                                  <option value="Bimbingan Teknis"  @if($peserta->jenis_pelatihan == 'Bimbingan Teknis') selected @endif>Bimbingan Teknis</option>


                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="jenis_usulan_formasi" class="col-sm-2 col-form-label">Jenis Usulan Formasi</label>
                  <div class="col-3">
                    <select name="jenis_usulan_formasi" class="form-control" id="jenis_usulan_formasi">
                      <option value="Pindah Jabatan formasi CPNS" @if($peserta->jenis_usulan_formasi == 'Pindah Jabatan formasi CPNS') selected @endif>Pindah Jabatan formasi CPNS</option>
                      <option value="Formasi CPNS" @if($peserta->jenis_usulan_formasi == 'Formasi CPNS') selected @endif>Formasi CPNS</option>

                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="form-label">Email</label>
                  <input value="{{$peserta->email}}" name="email" class="form-control input-date mb-2" type="email">
                </div>
                <div class="form-group">
                  <label class="form-label">No Telepone</label>
                  <input value="{{$peserta->no_telp}}" name="no_telp" class="form-control input-date mb-2" type="text">
                </div>
<div class="modal-footer">
<button type="submit" class="btn btn-primary">Edit</button></div>


</form>
</!-->
</div>
</div>

@endsection
