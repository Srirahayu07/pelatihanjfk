@extends('layout.master')
@section('content')
<div class="adminx-main-content">
  <div class="container-fluid">
    <!-- BreadCrumb -->
    <nav aria-label="breadcrumb" role="navigation">
      <ol class="breadcrumb adminx-page-breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item"><a href="/pelatihan">Pelatihan</a></li>
        <li class="breadcrumb-item active" aria-current="page">Tambah Usulan</li>
      </ol>
    </nav>

    <div class="pb-3">
      <h1>Tambah Usulan</h1>
    </div>
    <style media="screen">
      .putih{
        color:white;
      }
    </style>

    <form method="post" action="/pelatihan/store1">

      {{ csrf_field() }}
      <div class="row">
        <div class="col-lg-6">
          <div class="card mb-grid">
            <div class="card-header">
              <div class="card-header-title">Surat Usulan</div>
            </div>
            <div class="card-body">

              <div class="form-group">
                <label class="form-label">NIP</label>
                <input name="nip" class="form-control mb-2 input-credit-card" type="text" >
                <button type="submit" name="cari">Cari</button>
              </div>

              <div class="form-group">
                <label class="form-label">Nama</label>
                <input name="nama" class="form-control mb-2 input-credit-card" type="text">
              </div>

              <div class="form-group">
                <label class="form-label">TMT PNS</label>
                <input name="tmt_pns" class="form-control input-date mb-2" type="text" >
              </div>
              <div class="form-group">
                <label class="form-label">SK CPNS</label>
                <input name="sk_cpns" class="form-control input-date mb-2" type="text" >
              </div>
              <div class="form-group">
                <label class="form-label">SK PNS</label>
                <input name="sk_pns" class="form-control input-date mb-2" type="text" >
              </div>
              <div class="form-group">
                <label class="form-label">Pangkat</label>
                <input name="pangkat" class="form-control input-date mb-2" type="text" >
              </div>
              <div class="form-group">
                <label class="form-label">SK KP Terakhir</label>
                <input name="sk_kp_terakhir" class="form-control input-date mb-2" type="text">
              </div>
              <div class="form-group">
                <label class="form-label">Unit Kerja</label>
                <input name="unit_kerja" class="form-control input-date mb-2" type="text">
              </div>
              <div class="form-group">
                <label class="form-label">Pendidikan Terakhir</label>
                <input name="pendidikan_terakhir" class="form-control input-date mb-2" type="text">
              </div>
              <div class="form-group">
                <label class="form-label">Ijazah</label>
                <input name="ijazah" class="form-control input-date mb-2" type="text">
              </div>
              <div class="form-group row">
                <label for="jabatan" class="col-sm-2 col-form-label">Jabatan</label>
                <div class="col-3">
                  <select name="jabatan" class="form-control" id="jabatan" style="width: 250px">
                    <option selected disabled>-- Pilih Jabatan --</option>
                    <option value="Semua Jabatan">Semua Jabatan</option>
                    <option value="Analis Kepegawaian">Analis Kepegawaian</option>
                    <option value="Auditor Kepegawaian">Auditor Kepegawaian</option>

                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="jenis_pelatihan" class="col-sm-2 col-form-label">Jenis Pelatihan</label>
                <div class="col-3">
                  <select name="jenis_pelatihan" class="form-control" id="jenis_pelatihan" style="width: 250px">
                    <option selected disabled>-- Pilih Jenis Pelatihan --</option>
                    <option value="Diklat Fungsional Analis Kepegawaian Keterampilan">Diklat Fungsional Analis Kepegawaian Keterampilan</option>
                      <option value="Diklat Fungsional Analis Kepegawaian Keahlian">Diklat Fungsional Analis Kepegawaian Keahlian</option>
                        <option value="Diklat Fungsional Auditor Kepegawaian">Diklat Fungsional Auditor Kepegawaian</option>
                          <option value="Diklat Fungsional Assesor SDM Aparatur">Diklat Fungsional Assesor SDM Aparatur</option>
                            <option value="Diklat Teknis Kenaikan Pangkat">Diklat Teknis Kenaikan Pangkat</option>
                              <option value="Diklat Teknis Kompensasi">Diklat Teknis Kompensasi</option>
                                <option value="Diklat Teknis Tata Usaha Kepegawaian">Diklat Teknis Tata Usaha Kepegawaian</option>
                                  <option value="Diklat Teknis Disiplin PNS">Diklat Teknis Disiplin PNS</option>
                                    <option value="Diklat Teknis Pemberhentian dan Pensiun PNS">Diklat Teknis Pemberhentian dan Pensiun PNS</option>
                                      <option value="Diklat Teknis Pengadaan PNS">Diklat Teknis Pengadaan PNS</option>
                                        <option value="Diklat Teknis Pengembangan Karier PNS">Diklat Teknis Pengembangan Karier PNS</option>
                                          <option value="Diklat Teknis Penyusunan Analisis Jabatan">Diklat Teknis Penyusunan Analisis Jabatan</option>
                                            <option value="Diklat Teknis Penyusunan Standarisasi Jabatan PNS">Diklat Teknis Penyusunan Standarisasi Jabatan PNS</option>
                                              <option value="Diklat Teknis Penilaian dan Penetapan Angka Kredit">Diklat Teknis Penilaian dan Penetapan Angka Kredit</option>
                                                <option value="Diklat Teknis Penyusunan Rencana Kerja Wasdalpeg">Diklat Teknis Penyusunan Rencana Kerja Wasdalpeg</option>
                                                  <option value="Diklat Teknis Pelaksanaan Wasdalpeg">Diklat Teknis Pelaksanaan Wasdalpeg</option>
                                                    <option value="Diklat Teknis Pelaporan Hasil Wasdalpeg">Diklat Teknis Pelaporan Hasil Wasdalpeg</option>
                                                      <option value="Diklat Teknis Pemantauan Tindak Lanjut Hasil Wasdalpeg">Diklat Teknis Pemantauan Tindak Lanjut Hasil Wasdalpeg</option>
                                                        <option value="Diklat Teknis Evaluasi Bidang Wasdalpeg">Diklat Teknis Evaluasi Bidang Wasdalpeg</option>
                                                          <option value="Diklat Teknis Pembuatan Karya Tulis/Karya Ilmiah Bidang Wasdalpeg">Diklat Teknis Pembuatan Karya Tulis/Karya Ilmiah Bidang Wasdalpeg</option>
                                                            <option value="Diklat Penjenjangan Tingkat I">Diklat Penjenjangan Tingkat I</option>
                                                              <option value="Diklat Penjenjangan Tingkat II">Diklat Penjenjangan Tingkat II</option>
                                                                <option value="Bimbingan Teknis">Bimbingan Teknis</option>


                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label for="jenis_usulan_formasi" class="col-sm-2 col-form-label">Jenis Usulan Formasi</label>
                <div class="col-3">
                  <select name="jenis_usulan_formasi" class="form-control" id="jenis_usulan_formasi" style="width: 250px">
                    <option selected disabled>-- Pilih Jenis Usulan Formasi --</option>
                        <option value="Pindah Jabatan">Pindah Jabatan</option>
                            <option value="Formasi CPNS">Formasi CPNS</option>



                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="form-label">Email</label>
                <input name="email" class="form-control input-date mb-2" type="email">
              </div>
              <div class="form-group">
                <label class="form-label">No Telepone</label>
                <input name="no_telp" class="form-control input-date mb-2" type="text">
              </div>
              <div class="row">
                <div class="col-lg-12 text-center">
                  <button type="submit" class="btn-success">Simpan</button>
                    <button class="btn-danger"><a class="putih" href="{{url('pelatihan')}}">Batal</a></button>
                </div>

              </div>


            </div>
          </div>
        </div>
        </div>
    </form>

      </div>
    </div>
<hr>

    <div class="adminx-main-content">
      <div class="container-fluid">
    <div class="text-center">
        <h4>DAFTAR NAMA PNS CALON PESERTA DIKLAT FUNGSIONAL</h4>
        <h4>PEMERINTAH PROVINSI DKI JAKARTA</h4>
        <h4>NO SURAT: 813/9/5/2019</h4>
        <h4>TGL. SURAT: 20 MEI 2019</h4>
    </div>
    <div class="">
      <div class="table-responsive-md">
        <table class="table table-hover mb-0">
          <tr>
                    <th class=" text-center" rowspan="2">NO</th>
                    <th class=" text-center" rowspan="2">Jenis Diklat</th>

                    <th class=" text-center" rowspan="2">NIP</th>

                    <th class=" text-center" rowspan="2">Nama</th>
                    <th class=" text-center" rowspan="2">Pendidikan</th>
                    <th class=" text-center" rowspan="2">Pangkat</th>

                    <th class=" text-center" rowspan="2">Unit Kerja</th>
                    <th class=" text-center" rowspan="2">Jenis Usulan</th>
                    <th class=" text-center" rowspan="2">Email</th>

                    <th class=" text-center" rowspan="2">No. Telp/HP</th>
                    <th class=" text-center" colspan="4">Dokumen</th>
                    <th class=" text-center" rowspan="2">Tanggal Input</th>
                    <th class=" text-center" rowspan="2">Opsi</th>
                    <tr>
                      <th class=" text-center">Ijazah</th>
                      <th class=" text-center">SK CPNS</th>
                      <th class=" text-center">SK PNS</th>
                      <th class=" text-center">SK KP</th>
                    </tr>

                  </tr>
                  @php
                    $no = 1;
                  @endphp
                  @foreach ($peserta as $p)
                    <tr>
                      <td class=" text-center">{{ $no }}</td>
                      <td class=" text-center">{{ $p->jenis_pelatihan }}</td>
                      <td class=" text-center">{{ $p->nip }}</td>
                      <td class=" text-center">{{ $p->nama }}</td>
                      <th class=" text-center">{{ $p->pendidikan_terakhir}}</th>
                      <td class=" text-center">{{ $p->pangkat }}</td>
                      <td class=" text-center">{{ $p->unit_kerja }}</td>
                      <td class=" text-center">{{ $p->jenis_usulan_formasi }}</td>
                      <th class=" text-center">{{ $p->email}}</th>
                      <td class=" text-center">{{ $p->no_telp }}</td>
                      <td class=" text-center">{{ $p->ijazah }}</td>
                      <td class=" text-center">{{ $p->sk_cpns }}</td>
                      <th class=" text-center">{{ $p->sk_pns}}</th>
                      <td class=" text-center">{{ $p->sk_kp_terakhir }}</td>
                      <td class=" text-center">{{ $p->created_at }}</td>
                      <td class=" text-center">
                        <a href="/pelatihan/peserta/{{$p->id}}/edit" class=" btn btn-primary btn-sm">Edit</a>
                        <a href="/pelatihan/peserta/{{$p->id}}/delete" class=" btn btn-success btn-sm">Hapus</i></a>
                      </td>
                      @php
                        $no++;
                      @endphp
                    @endforeach
                    </tr>

        </table>
<br>
      </div>
    </div>
    <a href="#" class="btn btn-sm btn-danger">+ Tambah Calon Peserta</a>
    <a href="#" class="btn btn-sm btn-secondary">Ubah Data Surat</a>
    <a href="#" class="btn btn-sm btn-primary">Kirim</a>
  </div></div>
@endsection
